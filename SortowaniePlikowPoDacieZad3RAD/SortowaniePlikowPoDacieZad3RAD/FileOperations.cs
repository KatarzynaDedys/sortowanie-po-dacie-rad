﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortowaniePlikowPoDacieZad3RAD
{
    class FileOperations
    {
        DirectoryInfo info;
        List<FileInfo> fileList = new List<FileInfo>();

        public FileOperations(string path)
        {
            try
            {
                info = new DirectoryInfo(path);
                fileList = info.GetFiles().OrderBy(p => p.CreationTime).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine("Podano nie prawidłową ścieżkę do pliku");
                Console.ReadKey();
                Environment.Exit(1);
            }   
        }

        public void ChangeFileNames()
        {
            Console.WriteLine("Files before sort:");
            ShowFile();
            Console.WriteLine();
            int fileNumber = 1;
            fileList = info.GetFiles().OrderBy(p => p.CreationTime).ToList();
            foreach (FileInfo file in fileList)
            {
                File.Move(file.FullName, (file.DirectoryName + "\\" + fileNumber.ToString() + file.Extension));
                fileNumber++;
            }
            Console.WriteLine("Files after sort:");
            ShowFile();
        }

        public void ChangeFileNamesTemp()
        {
            int fileNumber = 1;
            List<FileInfo> fileTmpList = new List<FileInfo>();
            foreach (FileInfo file in fileList)
            {
                File.Move(file.FullName, (file.DirectoryName + "\\" + fileNumber.ToString() + "tmpFiles" + file.Extension));
                fileNumber++;
            }
        }

        private void ShowFile()
        {
            string[] fileListShow;
            fileListShow = Directory.GetFiles(info.FullName);
            for (int i = 0; i < fileListShow.Length; i++)
            {
                Console.WriteLine(fileListShow[i] + " " + File.GetLastWriteTime(info.FullName));
            }
        }
    }
}
