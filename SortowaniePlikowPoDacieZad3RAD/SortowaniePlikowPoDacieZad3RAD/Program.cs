﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace SortowaniePlikowPoDacieZad3RAD
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What path do you want?");
            string path = Console.ReadLine();
            FileOperations objects = new FileOperations(path);
            Console.WriteLine();
            objects.ChangeFileNamesTemp();
            objects.ChangeFileNames();
            Console.ReadKey();
        }
    }
}
